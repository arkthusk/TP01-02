from django.conf.urls import url
from .views import index,submit

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^submit/', submit, name='submit'),
    ]
