from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, submit
from .models import Status
from .forms import Form_Status

# Create your tests here.
class Lab5UnitTest(TestCase):

    def test_lab_5_url_is_exist(self):
        response = Client().get('/ustatus/')
        self.assertEqual(response.status_code, 200)

    def test_lab5_using_index_func(self):
        found = resolve('/ustatus/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_todo(self):
        # Creating a new activity
        new_activity = Status.objects.create(text='mengerjakan lab ppw')

        # Retrieving all available activity
        counting_all_available_todo = Status.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)


    def test_form_validation_for_blank_items(self):
        form = Form_Status(data={'text': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['text'],
            ["This field is required."]
        )
    def test_lab5_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/ustatus/submit/', {'text': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/ustatus/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_lab5_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/ustatus/submit/', {'text': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/ustatus/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)
