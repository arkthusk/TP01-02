from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Form_Status
from .models import Status
from templates.UserData import author, name

# Create your views here.
response={}
def index(request):
    status = Status.objects.all()
    response["nama"]=name
    response['form_status'] = Form_Status
    response['status']=status
    return render(request,'ustatus.html',response)
def submit(request):
    response['text'] = request.POST['text']
    status = Status(text=response['text'],)
    status.save()
    return HttpResponseRedirect('/ustatus/')
