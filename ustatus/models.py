from django.db import models

# Create your models here.
class Status (models.Model):
    text = models.CharField(max_length=140)
    created_date = models.DateTimeField(auto_now_add=True)
