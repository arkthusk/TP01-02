from django import forms

class Form_Status(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    status_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'placeholder':"What's going on...",
        'cols': 100,
        'rows': 4,
    }

    text = forms.CharField(label='', required=True, max_length=140, widget=forms.TextInput(attrs=status_attrs))
