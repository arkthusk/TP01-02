from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Message_Form
from .models import Message
# Create your views here.

response = {}

def index(request):
    message = Message.objects.all()
    response['message'] = message
    html = 'addfriend.html'
    return render(request, html, response)

def message_post(request):
    form = Message_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
        response['link'] = request.POST['link'] if request.POST['link'] != "" else "Anonymous"
        message = Message(name=response['name'], link=response['link'])
        message.save()
        html ='addfriend.html'
        return HttpResponseRedirect('/add-friend/')
    else:  
        return HttpResponseRedirect('/add-friend/')

def message_table(request):
    message = Message.objects.all()
    response['message'] = message
    html = 'addfriend.html'
    return render(request, html , response)