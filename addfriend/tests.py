from django.test import TestCase
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, message_post, message_table
from .forms import Message_Form
from .models import Message

# Create your tests here.
class AddFriendUnitTest(TestCase):
    def test_add_friend_url_is_exist(self):
	    response = Client().get('/add-friend/')
	    self.assertEqual(response.status_code, 200)

    def test_add_friend_using_index_func(self):
        found = resolve('/add-friend/')
        self.assertEqual(found.func, index) 

    def test_model_can_create_new_message(self):
        #Creating a new activity
        new_activity = Message.objects.create(name='joseph',link='joseph.herokuapp.com',)
        #Retrieving all available activity
        counting_all_available_message= Message.objects.all().count()
        self.assertEqual(counting_all_available_message,1)

    # def test_form_validation_for_blank_items(self):
    #     form = Message_Form(data={'name': '', 'link': ''})
    #     self.assertFalse(form.is_valid())
    #     self.assertEqual(
    #         form.errors['message'],
    #         ["Wajib Diisi"]
    #     )  

    def test_add_friend_post_fail(self):
        response = Client().get('/add-friend/')
        self.assertEqual(response.status_code, 200)

    def test_add_friend_post_success_and_render_the_result(self):
        anonymous = 'Anonymous'
        message = 'Anonymous'
        response = Client().post('/add-friend/add_message/', {'name': '', 'link': ''}, follow=True)
        self.assertEqual(response.status_code, 200)
        html_response = response.content.decode('utf8')
        self.assertIn(anonymous,html_response)
        self.assertIn(message,html_response)
	
    def test_add_friend_table_using_message_table_func(self):
        found = resolve('/add-friend/result_table')
        self.assertEqual(found.func, message_table)

    def test_add_friend_showing_all_messages(self):
        name_joseph = 'joseph'
        link_joseph = 'joseph.herokuapp.com'
        data_joseph = {'name': name_joseph, 'link': link_joseph}
        post_data_joseph = Client().post('/add-friend/', data_joseph)
        self.assertEqual(post_data_joseph.status_code, 200)

        data_anonymous = {'name': '', 'link': ''}
        post_data_anonymous = Client().post('/add-friend/', data_anonymous)
        self.assertEqual(post_data_anonymous.status_code, 200)

        response = Client().get('/add-friend/add_message/',follow = True)
        html_response = response.content.decode('utf8')

        for key,data in data_joseph.items():
            self.assertIn(data,html_response)

        self.assertIn(name_joseph, html_response)


