from django.conf.urls import url
from .views import statistic_count

urlpatterns = [
    url(r'^$', statistic_count, name='statistic_count'),
]
