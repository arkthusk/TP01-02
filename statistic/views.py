from django.shortcuts import render
from addfriend.models import Message
from ustatus.models import Status

response = {'author' : "Kelompok 2"}

# Create your views here.
def statistic_count(request):
	count_friend = Message.objects.all().count()
	response['count_friend'] = count_friend
	count_post = Status.objects.all().count()
	response['count_post'] = count_post


	html = 'statistic/statistic.html'
	qs = Status.objects.order_by('-created_date')
	if(len(qs) > 0):
		latest_post = qs[0]
	else:
		latest_post = None
	response['latest_post'] = latest_post
	
	return render(request, html, response) 




