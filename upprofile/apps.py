from django.apps import AppConfig


class UpprofileConfig(AppConfig):
    name = 'upprofile'
