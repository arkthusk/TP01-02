from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, author, name, content, expertise
from django.http import HttpRequest

    # Create your tests here.
class ProfilePageUnitTest(TestCase):

    def test_upprofile_url_is_exist(self):
        response = Client().get('/upprofil/')
        self.assertEqual(response.status_code, 200)

    def test_upprofile_using_index_func(self):
        found = resolve('/upprofil/')
        self.assertEqual(found.func, index)

    def test_profile_adding_content(self):
        self.assertIsNotNone(content)

        for profile in content:
            self.assertIsNotNone(profile['subject'])
            self.assertIsNotNone(profile['value'])
            self.assertIsNot(type(profile['value']), type(8))      
