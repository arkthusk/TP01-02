from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from templates.UserData import author, name
from datetime import datetime, date

birthday = date(2017,1,1)
gender = 'female'
expertise = 'Marketing, Collector, Public Speaking'
description = 'Antique expert. Experience as marketer for 10 years'
email = 'hello@smith.com'


content = [
{'subject' : 'Birthday' , 'value' : birthday.strftime('%d %B')},\
{'subject' : 'Gender' , 'value' : gender},\
{'subject' : 'Expertise' , 'value' : expertise},\
{'subject' : 'Description' , 'value' : description},\
{'subject' : 'Email' , 'value' : email},
]
# Create your views here.

def index(request):
    #TODO Implement, isilah dengan 6 kata yang mendeskripsikan anda
    html = 'upprofile/upprofile.html'
    response = {'author' : author, 'name' : name, 'content' : content, 'expertise' : expertise}
    return render(request, html, response)
